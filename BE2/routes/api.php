<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\SongsController;
use App\Http\Controllers\PlaylistsController;
use App\Http\Controllers\Playlist_SongController;



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();

});


//Songs route
Route::get('/songs', [SongsController::class, 'SongsDisplay']);
Route::post('/upload', [SongsController::class, 'store']);

//Playlist route
Route::get('/playlist', [PlaylistsController::class, 'PlaylistsDisplay']);
Route::post('/create', [PlaylistsController::class, 'store']);

//PlaylistSong route
Route::get('/playlistsongs', [Playlist_SongsController::class, 'SongPlaylistsDisplay']);
Route::post('/create', [Playlist_SongsController::class, 'store']);
