<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlists;

class PlaylistsController extends Controller
{
    public function PlaylistsDisplay() {
        return DB::table('playlists')->get();
    }

    //Create Playlist Controller
    public function store(Request $request) {

        $newPlaylists = new Playlists();
        $newPlaylists->Name = $request->Name;
        $newPlaylists->save();
        return $newPlaylists;
    }
}
