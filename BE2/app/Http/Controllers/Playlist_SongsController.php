<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Playlist_Songs;

class Playlist_SongsController extends Controller
{
    public function SongPlaylistsDisplay() {
        return DB::table('playlist__songs')->get();
    }

    //Create Song Playlist
    public function store(Request $request) {

        $newPlaylistSongs = new Playlist_Songs();
        $newPlaylistSongs->Song_ID = $request->Song_ID;
        $newPlaylistSongs->Playlist_ID = $request->Playlist_ID;
        $newPlaylistSongs->save();
        return $newPlaylistSongs;
    }
}
