<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Songs;

class SongsController extends Controller
{
    public function SongsDisplay() {
        return DB::table('songs')->get();
    }

    //Create Songs
    public function store(Request $request) {

        $newSongs = new Songs();
        $newSongs->Title = $request->Title;
        $newSongs->Length = $request->Length;
        $newSongs->Artist = $request->Artist;
        $newSongs->save();
        return $newSongs;
    }
}
